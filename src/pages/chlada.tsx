import React, { useState } from 'react';
import { Box, Text, Input, Button, Flex } from '@chakra-ui/react';

const Todos: React.FC = () => {
    const [data, changeData] = useState({
        data1: 'neco1',
        data2: 'neco2',
    })

    const sendData = (data: string) => {
        console.log(data, 'data')
    }

  return (
    <Flex
        flexFlow='column'
        justifyContent='center'
        h='100'
        w='100%'
        alignItems='center'
    >
        <Input
            placeholder='data1'
            onChange={(e) => changeData({
                ...data,
                data1: e.target.value
            })}
            w='200px'
        />
        <Input
            placeholder='data2'
            onChange={(e) => changeData({
                ...data,
                data2: e.target.value
            })}
            w='200px'
        />
        <Button onClick={() => sendData(`data1: ${data.data1}\ndata2: ${data.data2}`)} w='200px'>Odeslat</Button>
        <Text>
            {
                JSON.stringify(data)
            }
        </Text>
    </Flex>
  );
}

export default Todos;
