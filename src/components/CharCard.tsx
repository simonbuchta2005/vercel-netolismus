import React, { useEffect, useState } from 'react'
import { Box, Card, Flex, Heading, Icon, IconButton, SimpleGrid, Text, useToast } from "@chakra-ui/react";
export interface CardType {
    title: string,
    desc: string,
    icon: any,
  }
  
  interface CardProps {
    card: CardType,
    idx: number
  }
  

const CharCard: React.FC<CardProps> = ({ card, idx }) => {
    return (
        <Flex
            w='180px'
            h='160px'
            borderRadius='3xl'
            background='white'
            p='15px'
            data-aos-delay={(idx + 1) * 250}
            data-aos="fade"
            data-aos-easing="linear"
            data-aos-duration="250"
        >
            <Box>
                <Text
                    fontWeight='extrabold'
                    textTransform='uppercase'
                    color='blackAlpha.700'
                >
                    {card.title}
                </Text>
                <Text
                    fontSize='sm'
                    color='blackAlpha.600'
                >
                    {card.desc}
                </Text>
            </Box>
            <Box
                width='15px'
                ml='5px'
                borderRadius='xl'
                bgGradient='linear(to-t, #D74177, #FFE98A)'
            />
        </Flex>
    )
}

export default CharCard;