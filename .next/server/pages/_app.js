"use strict";
/*
 * ATTENTION: An "eval-source-map" devtool has been used.
 * This devtool is neither made for production nor for readable output files.
 * It uses "eval()" calls to create a separate source file with attached SourceMaps in the browser devtools.
 * If you are trying to read the output file, select a different devtool (https://webpack.js.org/configuration/devtool/)
 * or disable the default devtool with "devtool: false".
 * If you are looking for production-ready output files, see mode: "production" (https://webpack.js.org/configuration/mode/).
 */
(() => {
var exports = {};
exports.id = "pages/_app";
exports.ids = ["pages/_app"];
exports.modules = {

/***/ "./src/pages/_app.tsx":
/*!****************************!*\
  !*** ./src/pages/_app.tsx ***!
  \****************************/
/***/ ((module, __webpack_exports__, __webpack_require__) => {

eval("__webpack_require__.a(module, async (__webpack_handle_async_dependencies__, __webpack_async_result__) => { try {\n__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"default\": () => (__WEBPACK_DEFAULT_EXPORT__)\n/* harmony export */ });\n/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-dev-runtime */ \"react/jsx-dev-runtime\");\n/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var styled_jsx_style__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! styled-jsx/style */ \"styled-jsx/style\");\n/* harmony import */ var styled_jsx_style__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(styled_jsx_style__WEBPACK_IMPORTED_MODULE_1__);\n/* harmony import */ var _chakra_ui_react__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @chakra-ui/react */ \"@chakra-ui/react\");\n/* harmony import */ var next_seo__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! next-seo */ \"next-seo\");\n/* harmony import */ var next_seo__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(next_seo__WEBPACK_IMPORTED_MODULE_3__);\n/* harmony import */ var chart_js__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! chart.js */ \"chart.js\");\n/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! react */ \"react\");\n/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_5__);\n/* harmony import */ var aos_dist_aos_css__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! aos/dist/aos.css */ \"./node_modules/aos/dist/aos.css\");\n/* harmony import */ var aos_dist_aos_css__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(aos_dist_aos_css__WEBPACK_IMPORTED_MODULE_6__);\n/* harmony import */ var aos__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! aos */ \"aos\");\n/* harmony import */ var aos__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(aos__WEBPACK_IMPORTED_MODULE_7__);\nvar __webpack_async_dependencies__ = __webpack_handle_async_dependencies__([_chakra_ui_react__WEBPACK_IMPORTED_MODULE_2__, chart_js__WEBPACK_IMPORTED_MODULE_4__]);\n([_chakra_ui_react__WEBPACK_IMPORTED_MODULE_2__, chart_js__WEBPACK_IMPORTED_MODULE_4__] = __webpack_async_dependencies__.then ? (await __webpack_async_dependencies__)() : __webpack_async_dependencies__);\n\n\n\n\n\n\n\n\nchart_js__WEBPACK_IMPORTED_MODULE_4__.Chart.register(chart_js__WEBPACK_IMPORTED_MODULE_4__.CategoryScale, chart_js__WEBPACK_IMPORTED_MODULE_4__.LinearScale, chart_js__WEBPACK_IMPORTED_MODULE_4__.BarElement);\nfunction MyApp({ Component, pageProps }) {\n    (0,react__WEBPACK_IMPORTED_MODULE_5__.useEffect)(()=>{\n        aos__WEBPACK_IMPORTED_MODULE_7___default().init();\n    }, []);\n    const theme = (0,_chakra_ui_react__WEBPACK_IMPORTED_MODULE_2__.extendTheme)({\n        styles: {\n            global: ()=>({\n                    body: {\n                        bg: \"#ffffff\"\n                    },\n                    colors: {\n                        myPink: {\n                            50: \"#fde2e8\",\n                            100: \"#fbb6c9\",\n                            200: \"#f98ba0\",\n                            300: \"#f65f77\",\n                            400: \"#f4334e\",\n                            500: \"#d74177\",\n                            600: \"#b02c5e\",\n                            700: \"#891744\",\n                            800: \"#62032b\",\n                            900: \"#3e0011\"\n                        }\n                    },\n                    breakpoints: {\n                        sm: \"320px\",\n                        md: \"768px\",\n                        lg: \"1440px\",\n                        xl: \"1441px\"\n                    }\n                })\n        }\n    });\n    return /*#__PURE__*/ (0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(_chakra_ui_react__WEBPACK_IMPORTED_MODULE_2__.ChakraProvider, {\n        theme: theme,\n        children: /*#__PURE__*/ (0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(_chakra_ui_react__WEBPACK_IMPORTED_MODULE_2__.ColorModeProvider, {\n            options: {\n                initialColorMode: \"light\",\n                useSystemColorMode: false\n            },\n            children: [\n                /*#__PURE__*/ (0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(next_seo__WEBPACK_IMPORTED_MODULE_3__.DefaultSeo, {\n                    titleTemplate: \"Netolismus\",\n                    additionalLinkTags: [\n                        {\n                            rel: \"icon\",\n                            href: \"/globe.svg\"\n                        }\n                    ],\n                    defaultTitle: \"Netolismus\",\n                    description: \"Netolismus je z\\xe1vislot na tzv. virtu\\xe1ln\\xedch drog\\xe1ch.\",\n                    openGraph: {\n                        title: \"Netolismus\",\n                        description: \"Netolismus je z\\xe1vislot na tzv. virtu\\xe1ln\\xedch drog\\xe1ch.\"\n                    }\n                }, void 0, false, {\n                    fileName: \"/Applications/Development/TODOs/vercel-netolismus/src/pages/_app.tsx\",\n                    lineNumber: 51,\n                    columnNumber: 11\n                }, this),\n                (0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)((styled_jsx_style__WEBPACK_IMPORTED_MODULE_1___default()), {\n                    id: \"862fcb508885fca4\",\n                    children: \"body{margin:0}\"\n                }, void 0, false, void 0, this),\n                /*#__PURE__*/ (0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(Component, {\n                    ...pageProps,\n                    className: \"jsx-862fcb508885fca4\" + \" \" + (pageProps && pageProps.className != null && pageProps.className || \"\")\n                }, void 0, false, {\n                    fileName: \"/Applications/Development/TODOs/vercel-netolismus/src/pages/_app.tsx\",\n                    lineNumber: 71,\n                    columnNumber: 11\n                }, this)\n            ]\n        }, void 0, true, {\n            fileName: \"/Applications/Development/TODOs/vercel-netolismus/src/pages/_app.tsx\",\n            lineNumber: 50,\n            columnNumber: 9\n        }, this)\n    }, void 0, false, {\n        fileName: \"/Applications/Development/TODOs/vercel-netolismus/src/pages/_app.tsx\",\n        lineNumber: 47,\n        columnNumber: 7\n    }, this);\n}\n/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (MyApp);\n\n__webpack_async_result__();\n} catch(e) { __webpack_async_result__(e); } });//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvcGFnZXMvX2FwcC50c3giLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBa0Y7QUFFNUM7QUFDbUM7QUFDdkM7QUFDUjtBQUNKO0FBRXRCSSwyQ0FBS0EsQ0FBQ00sUUFBUSxDQUFDTCxtREFBYUEsRUFBRUMsaURBQVdBLEVBQUVDLGdEQUFVQTtBQUVyRCxTQUFTSSxNQUFNLEVBQUVDLFNBQVMsRUFBRUMsU0FBUyxFQUFZO0lBQy9DTCxnREFBU0EsQ0FBQztRQUNSQywrQ0FBUTtJQUNWLEdBQUcsRUFBRTtJQUVMLE1BQU1NLFFBQVFiLDZEQUFXQSxDQUFDO1FBQ3hCYyxRQUFRO1lBQ05DLFFBQVEsSUFBTztvQkFDYkMsTUFBTTt3QkFDSkMsSUFBSTtvQkFDTjtvQkFDQUMsUUFBUTt3QkFDTkMsUUFBUTs0QkFDTixJQUFJOzRCQUNKLEtBQUs7NEJBQ0wsS0FBSzs0QkFDTCxLQUFLOzRCQUNMLEtBQUs7NEJBQ0wsS0FBSzs0QkFDTCxLQUFLOzRCQUNMLEtBQUs7NEJBQ0wsS0FBSzs0QkFDTCxLQUFLO3dCQUNQO29CQUNGO29CQUNBQyxhQUFhO3dCQUNYQyxJQUFJO3dCQUNKQyxJQUFJO3dCQUNKQyxJQUFJO3dCQUNKQyxJQUFJO29CQUNOO2dCQUNGO1FBQ0Y7SUFDRjtJQUVFLHFCQUNFLDhEQUFDMUIsNERBQWNBO1FBQ2JlLE9BQU9BO2tCQUVQLDRFQUFDZCwrREFBaUJBO1lBQUMwQixTQUFTO2dCQUFFQyxrQkFBa0I7Z0JBQVNDLG9CQUFvQjtZQUFNOzs4QkFDakYsOERBQUMxQixnREFBVUE7b0JBQ1AyQixlQUFjO29CQUNkQyxvQkFBb0I7d0JBQ2xCOzRCQUNFQyxLQUFLOzRCQUNMQyxNQUFNO3dCQUNSO3FCQUNEO29CQUNEQyxjQUFhO29CQUNiQyxhQUFZO29CQUNaQyxXQUFXO3dCQUNQQyxPQUFPO3dCQUNQRixhQUFhO29CQUNqQjs7Ozs7Ozs7Ozs4QkFPSiw4REFBQ3ZCO29CQUFXLEdBQUdDLFNBQVM7K0RBQVRBLGFBQUFBLCtCQUFBQTs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFJekI7QUFFQSxpRUFBZUYsS0FBS0EsRUFBQyIsInNvdXJjZXMiOlsid2VicGFjazovL25ldG9saXNtdXMtd2ViLy4vc3JjL3BhZ2VzL19hcHAudHN4P2Y5ZDYiXSwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ2hha3JhUHJvdmlkZXIsIENvbG9yTW9kZVByb3ZpZGVyLCBleHRlbmRUaGVtZSB9IGZyb20gJ0BjaGFrcmEtdWkvcmVhY3QnO1xuaW1wb3J0IHR5cGUgeyBBcHBQcm9wcyB9IGZyb20gJ25leHQvYXBwJztcbmltcG9ydCB7IERlZmF1bHRTZW8gfSBmcm9tICduZXh0LXNlbyc7XG5pbXBvcnQgeyBDaGFydCwgQ2F0ZWdvcnlTY2FsZSwgTGluZWFyU2NhbGUsIEJhckVsZW1lbnQgfSBmcm9tICdjaGFydC5qcyc7XG5pbXBvcnQgeyB1c2VFZmZlY3QgfSBmcm9tICdyZWFjdCc7XG5pbXBvcnQgJ2Fvcy9kaXN0L2Fvcy5jc3MnO1xuaW1wb3J0IEFPUyBmcm9tICdhb3MnO1xuXG5DaGFydC5yZWdpc3RlcihDYXRlZ29yeVNjYWxlLCBMaW5lYXJTY2FsZSwgQmFyRWxlbWVudCk7XG5cbmZ1bmN0aW9uIE15QXBwKHsgQ29tcG9uZW50LCBwYWdlUHJvcHMgfTogQXBwUHJvcHMpIHtcbiAgdXNlRWZmZWN0KCgpID0+IHtcbiAgICBBT1MuaW5pdCgpO1xuICB9LCBbXSk7XG5cbiAgY29uc3QgdGhlbWUgPSBleHRlbmRUaGVtZSh7XG4gICAgc3R5bGVzOiB7XG4gICAgICBnbG9iYWw6ICgpID0+ICh7XG4gICAgICAgIGJvZHk6IHtcbiAgICAgICAgICBiZzogJyNmZmZmZmYnLFxuICAgICAgICB9LFxuICAgICAgICBjb2xvcnM6IHtcbiAgICAgICAgICBteVBpbms6IHtcbiAgICAgICAgICAgIDUwOiAnI2ZkZTJlOCcsXG4gICAgICAgICAgICAxMDA6ICcjZmJiNmM5JyxcbiAgICAgICAgICAgIDIwMDogJyNmOThiYTAnLFxuICAgICAgICAgICAgMzAwOiAnI2Y2NWY3NycsXG4gICAgICAgICAgICA0MDA6ICcjZjQzMzRlJyxcbiAgICAgICAgICAgIDUwMDogJyNkNzQxNzcnLFxuICAgICAgICAgICAgNjAwOiAnI2IwMmM1ZScsXG4gICAgICAgICAgICA3MDA6ICcjODkxNzQ0JyxcbiAgICAgICAgICAgIDgwMDogJyM2MjAzMmInLFxuICAgICAgICAgICAgOTAwOiAnIzNlMDAxMScsXG4gICAgICAgICAgfSxcbiAgICAgICAgfSxcbiAgICAgICAgYnJlYWtwb2ludHM6IHtcbiAgICAgICAgICBzbTogJzMyMHB4JyxcbiAgICAgICAgICBtZDogJzc2OHB4JyxcbiAgICAgICAgICBsZzogJzE0NDBweCcsXG4gICAgICAgICAgeGw6ICcxNDQxcHgnLFxuICAgICAgICB9LFxuICAgICAgfSksXG4gICAgfSxcbiAgfSk7XG4gIFxuICAgIHJldHVybiAoXG4gICAgICA8Q2hha3JhUHJvdmlkZXJcbiAgICAgICAgdGhlbWU9e3RoZW1lfVxuICAgICAgPlxuICAgICAgICA8Q29sb3JNb2RlUHJvdmlkZXIgb3B0aW9ucz17eyBpbml0aWFsQ29sb3JNb2RlOiAnbGlnaHQnLCB1c2VTeXN0ZW1Db2xvck1vZGU6IGZhbHNlIH19PlxuICAgICAgICAgIDxEZWZhdWx0U2VvXG4gICAgICAgICAgICAgIHRpdGxlVGVtcGxhdGU9J05ldG9saXNtdXMnXG4gICAgICAgICAgICAgIGFkZGl0aW9uYWxMaW5rVGFncz17W1xuICAgICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICAgIHJlbDogJ2ljb24nLFxuICAgICAgICAgICAgICAgICAgaHJlZjogJy9nbG9iZS5zdmcnLFxuICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgIF19XG4gICAgICAgICAgICAgIGRlZmF1bHRUaXRsZT0nTmV0b2xpc211cydcbiAgICAgICAgICAgICAgZGVzY3JpcHRpb249J05ldG9saXNtdXMgamUgesOhdmlzbG90IG5hIHR6di4gdmlydHXDoWxuw61jaCBkcm9nw6FjaC4nXG4gICAgICAgICAgICAgIG9wZW5HcmFwaD17e1xuICAgICAgICAgICAgICAgICAgdGl0bGU6ICdOZXRvbGlzbXVzJyxcbiAgICAgICAgICAgICAgICAgIGRlc2NyaXB0aW9uOiAnTmV0b2xpc211cyBqZSB6w6F2aXNsb3QgbmEgdHp2LiB2aXJ0dcOhbG7DrWNoIGRyb2fDoWNoLicsXG4gICAgICAgICAgICAgIH19XG4gICAgICAgICAgLz5cbiAgICAgICAgICA8c3R5bGUganN4IGdsb2JhbD57YFxuICAgICAgICAgICAgYm9keSB7XG4gICAgICAgICAgICAgIG1hcmdpbjogMDtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICBgfTwvc3R5bGU+XG4gICAgICAgICAgPENvbXBvbmVudCB7Li4ucGFnZVByb3BzfSAvPlxuICAgICAgPC9Db2xvck1vZGVQcm92aWRlcj5cbiAgICA8L0NoYWtyYVByb3ZpZGVyPlxuICApO1xufVxuXG5leHBvcnQgZGVmYXVsdCBNeUFwcDtcblxuZGVjbGFyZSBnbG9iYWwge1xuICBpbnRlcmZhY2UgV2luZG93IHtcbiAgICBHQV9JTklUSUFMSVpFRDogYm9vbGVhbjtcbiAgfVxufSJdLCJuYW1lcyI6WyJDaGFrcmFQcm92aWRlciIsIkNvbG9yTW9kZVByb3ZpZGVyIiwiZXh0ZW5kVGhlbWUiLCJEZWZhdWx0U2VvIiwiQ2hhcnQiLCJDYXRlZ29yeVNjYWxlIiwiTGluZWFyU2NhbGUiLCJCYXJFbGVtZW50IiwidXNlRWZmZWN0IiwiQU9TIiwicmVnaXN0ZXIiLCJNeUFwcCIsIkNvbXBvbmVudCIsInBhZ2VQcm9wcyIsImluaXQiLCJ0aGVtZSIsInN0eWxlcyIsImdsb2JhbCIsImJvZHkiLCJiZyIsImNvbG9ycyIsIm15UGluayIsImJyZWFrcG9pbnRzIiwic20iLCJtZCIsImxnIiwieGwiLCJvcHRpb25zIiwiaW5pdGlhbENvbG9yTW9kZSIsInVzZVN5c3RlbUNvbG9yTW9kZSIsInRpdGxlVGVtcGxhdGUiLCJhZGRpdGlvbmFsTGlua1RhZ3MiLCJyZWwiLCJocmVmIiwiZGVmYXVsdFRpdGxlIiwiZGVzY3JpcHRpb24iLCJvcGVuR3JhcGgiLCJ0aXRsZSJdLCJzb3VyY2VSb290IjoiIn0=\n//# sourceURL=webpack-internal:///./src/pages/_app.tsx\n");

/***/ }),

/***/ "aos":
/*!**********************!*\
  !*** external "aos" ***!
  \**********************/
/***/ ((module) => {

module.exports = require("aos");

/***/ }),

/***/ "next-seo":
/*!***************************!*\
  !*** external "next-seo" ***!
  \***************************/
/***/ ((module) => {

module.exports = require("next-seo");

/***/ }),

/***/ "react":
/*!************************!*\
  !*** external "react" ***!
  \************************/
/***/ ((module) => {

module.exports = require("react");

/***/ }),

/***/ "react/jsx-dev-runtime":
/*!****************************************!*\
  !*** external "react/jsx-dev-runtime" ***!
  \****************************************/
/***/ ((module) => {

module.exports = require("react/jsx-dev-runtime");

/***/ }),

/***/ "styled-jsx/style":
/*!***********************************!*\
  !*** external "styled-jsx/style" ***!
  \***********************************/
/***/ ((module) => {

module.exports = require("styled-jsx/style");

/***/ }),

/***/ "@chakra-ui/react":
/*!***********************************!*\
  !*** external "@chakra-ui/react" ***!
  \***********************************/
/***/ ((module) => {

module.exports = import("@chakra-ui/react");;

/***/ }),

/***/ "chart.js":
/*!***************************!*\
  !*** external "chart.js" ***!
  \***************************/
/***/ ((module) => {

module.exports = import("chart.js");;

/***/ })

};
;

// load runtime
var __webpack_require__ = require("../webpack-runtime.js");
__webpack_require__.C(exports);
var __webpack_exec__ = (moduleId) => (__webpack_require__(__webpack_require__.s = moduleId))
var __webpack_exports__ = __webpack_require__.X(0, ["vendor-chunks/aos"], () => (__webpack_exec__("./src/pages/_app.tsx")));
module.exports = __webpack_exports__;

})();